/**********************************************************************************************
 * Arduino PID Library - Version 1.1.31
 * by Brett Beauregard <br3ttb@gmail.com> brettbeauregard.com
 *
 * This Library is licensed under a GPLv3 License
 **********************************************************************************************/

#pragma once

#define LIBRARY_VERSION 1.1.3

namespace brettbeauregard {

enum Mode {MANUAL=0,AUTOMATIC=1};
enum Direction {DIRECT=0,REVERSE=1};

template <typename T, const int MINLIMIT=0, const int MAXLIMIT=255, const uint32_t SAMPLETIME=100>
class PIDT{
  // prevent strange errors due to implicit conversions etc.
  PIDT(const PIDT<T>& other) = delete; // non construction-copyable
  PIDT& operator=(const PIDT<T>&) = delete; // non copyable
  PIDT() = delete;

public:

  //commonly used functions **************************************************************************

  /*Constructor (...)*********************************************************
   *    The parameters specified here are those for for which we can't set up
   *    reliable defaults, so we need to have the user set them.
   ***************************************************************************/
  PIDT(T (*calculateInput)(uint32_t timeChange),
       void (*processOutput)(T output),
       T* setpoint, T kp, T ki, T kd, Direction controllerDirection):
         _mySetpoint(setpoint),
         _inAuto(false),
         _myCalculateInput(calculateInput),
         _myProcessOutput(processOutput){

    setOutputLimits(MINLIMIT, MAXLIMIT);   //default output limit corresponds to
    //the arduino pwm limits

    _sampleTime = SAMPLETIME;               //default Controller Sample Time is 0.1 seconds

    setControllerDirection(controllerDirection);
    setTunings(kp, ki, kd);

    _lastTime = millis()-_sampleTime;
    initialize();
}

  /* compute() **********************************************************************
   *     This, as they say, is where the magic happens.  this function should be called
   *   every time "void loop()" executes.  the function will decide for itself whether a new
   *   pid Output needs to be computed.  returns true when the output is computed,
   *   false when nothing has been done.
   **********************************************************************************/
  bool compute(){
    if(!_inAuto) return false;
    unsigned long now = millis();
    unsigned long timeChange = (now - _lastTime);
    if(timeChange>=_sampleTime){
      /*compute all the working error variables*/
      T input = _myCalculateInput(timeChange);
      T error = *_mySetpoint - input;
      _iTerm+= (_ki * error);
      if(_iTerm > _outMax) _iTerm= _outMax;
      else if(_iTerm < _outMin) _iTerm= _outMin;
      T dInput = (input - _lastInput);

      /*compute PIDT Output*/
      T output = _kp * error + _iTerm - _kd * dInput;

      if(output > _outMax) output = _outMax;
      else if(output < _outMin) output = _outMin;
      _lastOutput = output;
      _myProcessOutput(output);

      /*Remember some variables for next time*/
      _lastInput = input;
      _lastTime = now;
      return true;
    } else
      return false;
  }

  /* compute() **********************************************************************
   *     This, as they say, is where the magic happens.  this function should be called
   *   every time "void loop()" executes.  the function will decide for itself whether a new
   *   pid Output needs to be computed.  returns true when the output is computed,
   *   false when nothing has been done.
   **********************************************************************************/
  bool compute(bool debug){
    if(!_inAuto) return false;
    unsigned long now = millis();
    unsigned long timeChange = (now - _lastTime);
    if(timeChange>=_sampleTime){
      /*compute all the working error variables*/
      T input = _myCalculateInput(timeChange);
      T error = *_mySetpoint - input;
      _iTerm+= (_ki * error);
      if(_iTerm > _outMax) _iTerm= _outMax;
      else if(_iTerm < _outMin) _iTerm= _outMin;
      T dInput = (input - _lastInput);

      /*compute PIDT Output*/
      T output = _kp * error + _iTerm - _kd * dInput;
      if (debug){
        Serial.print("mySetpoint :");
        Serial.print(*_mySetpoint);
        Serial.print(" input :");
        Serial.print(input);
        Serial.print(" kp:");
        Serial.print(_kp);
        Serial.print(" ki:");
        Serial.print(_ki);
        Serial.print(" kd:");
        Serial.print(_kd);
        Serial.print(" kp * error:");
        Serial.print(_kp * error);
        Serial.print(" iTerm:");
        Serial.print(_iTerm);
        Serial.print(" kd * dInput:");
        Serial.println(_kd * dInput);
      }

      if(output > _outMax) output = _outMax;
      else if(output < _outMin) output = _outMin;
      _lastOutput = output;
      _myProcessOutput(output);

      /*Remember some variables for next time*/
      _lastInput = input;
      _lastTime = now;
      return true;
    } else
      return false;
  }

  /* setTunings(...)*************************************************************
   * This function allows the controller's dynamic performance to be adjusted.
   * it's called automatically from the constructor, but tunings can also
   * be adjusted on the fly during normal operation
   ******************************************************************************/

  void setTunings(T p, T i, T d){
    if (p<0 || i<0 || d<0) return;  // silently ignore error :/

    _dispKp = p; _dispKi = i; _dispKd = d;

    T SampleTimeInSec = ((T)_sampleTime)/1000;
    _kp = p;
    _ki = i * SampleTimeInSec;
    _kd = d / SampleTimeInSec;

    if(_controllerDirection==REVERSE){
      _kp = -_kp;
      _ki = -_ki;
      _kd = -_kd;
    }
  }

  /* setSampleTime(...) *********************************************************
   * sets the period, in Milliseconds, at which the calculation is performed
   ******************************************************************************/

  void setSampleTime(uint32_t newSampleTime){
    if (newSampleTime > 0){
      T ratio  = (T)newSampleTime / (T)_sampleTime;
      _ki *= ratio;
      _kd /= ratio;
      _sampleTime = (unsigned long)newSampleTime;
    }
  }

  /* getSampleTime(...) *********************************************************
   * returns the sample period, in Milliseconds, at which the calculation is performed
   ******************************************************************************/

  uint32_t getSampleTime(){
    return _sampleTime;
  }
  
  /* setOutputLimits(...)****************************************************
   *     This function will be used far more often than SetInputLimits.  while
   *  the input to the controller will generally be in the 0-1023 range (which is
   *  the default already,)  the output will be a little different.  maybe they'll
   *  be doing a time window and will need 0-8000 or something.  or maybe they'll
   *  want to clamp it from 0-125.  who knows.  at any rate, that can all be done
   *  here.
   **************************************************************************/

  void setOutputLimits(T Min, T Max){
    if(Min >= Max) return; // silently ignore error condition?
    _outMin = Min;
    _outMax = Max;

    if(_inAuto){
      if(_lastOutput > _outMax) _lastOutput = _outMax;
      else if(_lastOutput < _outMin) _lastOutput = _outMin;

      if(_iTerm > _outMax) _iTerm= _outMax;
      else if(_iTerm < _outMin) _iTerm= _outMin;
      _myProcessOutput(_lastOutput);
    }
  }

  /* setMode(...)****************************************************************
   * Allows the controller Mode to be set to manual (0) or Automatic (non-zero)
   * when the transition from manual to auto occurs, the controller is
   * automatically initialized
   ******************************************************************************/

  void setMode(Mode mode)
  {
    bool newAuto = (mode==AUTOMATIC);
    if(newAuto && !_inAuto){
      /*we just went from manual to auto*/
      initialize();
    }
    _inAuto = newAuto;
  }

  /* setControllerDirection(...)*************************************************
   * The PIDT will either be connected to a DIRECT acting process (+Output leads
   * to +input) or a REVERSE acting process(+Output leads to -input.)  we need to
   * know which one, because otherwise we may increase the output when we should
   * be decreasing.  This is called from the constructor.
   ******************************************************************************/

  void setControllerDirection(Direction direction){
    if(_inAuto && direction!=_controllerDirection){
      _kp = -_kp;
      _ki = -_ki;
      _kd = -_kd;
    }
    _controllerDirection = direction;
  }

  /* Status Funcions*************************************************************
   * Just because you set the kp=-1 doesn't mean it actually happened.  these
   * functions query the internal state of the PID.  they're here for display
   * purposes.  this are the functions the PID Front-end uses for example
   ******************************************************************************/

  T getKp(){ return _dispKp;}
  T getKi(){ return _dispKi;}
  T getKd(){ return _dispKd;}
  Mode getMode(){ return  _inAuto ? AUTOMATIC : MANUAL;}
  Direction getControllerDirection(){ return _controllerDirection;}

private:
  /* initialize()****************************************************************
   *  does all the things that need to happen to ensure a bumpless transfer
   *  from manual to automatic mode.
   ******************************************************************************/

  void initialize(){
    _iTerm = _lastOutput;
    _lastInput = _myCalculateInput(_sampleTime);
    if(_iTerm > _outMax) _iTerm = _outMax;
    else if(_iTerm < _outMin) _iTerm = _outMin;
  }

  T _dispKp;               // * we'll hold on to the tuning parameters in user-entered
  T _dispKi;               //   format for display purposes
  T _dispKd;               //

  T _kp;                  // * (P)roportional Tuning Parameter
  T _ki;                  // * (I)ntegral Tuning Parameter
  T _kd;                  // * (D)erivative Tuning Parameter

  Direction _controllerDirection;

  T *_mySetpoint;           // * Pointer to the Setpoint variable
  //   This creates a hard link between the variable and the
  //   PID, freeing the user from having to constantly tell us
  //   what that value is.  with pointers we'll just know.

  uint32_t _lastTime;
  T _iTerm, _lastInput, _lastOutput;

  uint32_t _sampleTime;
  T _outMin, _outMax;
  bool _inAuto;

  T (*_myCalculateInput)(uint32_t timeChange) = nullptr;
  void (*_myProcessOutput)(T output) = nullptr;

};

typedef PIDT<double> PID;

} // namespace brettbeauregard

