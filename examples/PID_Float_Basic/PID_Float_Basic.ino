/********************************************************
 * PID Basic Example using float instead of double
 * 
 * Reading analog input 0 to control analog PWM output 3
 ********************************************************/

#include <brettbeauregard_pid.h>

#define PIN_INPUT 0
#define PIN_OUTPUT 3

//Define Variables we'll be connecting to
float setpoint;

float calculateInput(uint32_t) {
  return analogRead(PIN_INPUT);
}

void processOutput(float output) {
  analogWrite(PIN_OUTPUT, output);
}

//Specify the links and initial tuning parameters
float kp=2, ki=5, kd=1;
brettbeauregard::PIDT<float> myPID(calculateInput, processOutput, &setpoint, kp, ki, kd, brettbeauregard::DIRECT);

void setup(){
  setpoint = 100;

  //turn the PID on
  myPID.setMode(brettbeauregard::AUTOMATIC);
}

void loop(){
  myPID.compute();
}


