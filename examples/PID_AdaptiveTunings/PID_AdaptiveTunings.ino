/********************************************************
 * PID Adaptive Tuning Example
 * One of the benefits of the PID library is that you can
 * change the tuning parameters at any time.  this can be
 * helpful if we want the controller to be agressive at some
 * times, and conservative at others.   in the example below
 * we set the controller to use Conservative Tuning Parameters
 * when we're near setpoint and more agressive Tuning
 * Parameters when we're farther away.
 ********************************************************/

#include <brettbeauregard_pid.h>

#define PIN_INPUT 0
#define PIN_OUTPUT 3
typedef float Real;

using namespace brettbeauregard;

//Define Variables we'll be connecting to
Real setpoint, input, output;

Real calculateInput(uint32_t) {
  input = analogRead(PIN_INPUT);
  return input;
}

void processOutput(Real output) {
  analogWrite(PIN_OUTPUT, output);
}

//Define the aggressive and conservative Tuning Parameters
Real aggKp=4, aggKi=0.2, aggKd=1;
Real consKp=1, consKi=0.05, consKd=0.25;

//Specify the links and initial tuning parameters
PIDT<Real> myPID(calculateInput, processOutput, &setpoint, consKp, consKi, consKd, DIRECT);

void setup(){
  //initialize the variables we're linked to
  setpoint = 100;

  //turn the PID on
  myPID.setMode(AUTOMATIC);
}

void loop(){
 
  Real gap = abs(setpoint-input); //distance away from setpoint
  if (gap < 10) {  
    //we're close to setpoint, use conservative tuning parameters
    myPID.setTunings(consKp, consKi, consKd);
  } else {
    //we're far from setpoint, use aggressive tuning parameters
    myPID.setTunings(aggKp, aggKi, aggKd);
  }

  myPID.compute();
}

