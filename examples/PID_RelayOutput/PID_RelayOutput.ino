/********************************************************
 * PID RelayOutput Example
 * Same as basic example, except that this time, the output
 * is going to a digital pin which (we presume) is controlling
 * a relay.  the pid is designed to output an analog value,
 * but the relay can only be On/Off.
 *
 *   to connect them together we use "time proportioning
 * control"  it's essentially a really slow version of PWM.
 * first we decide on a window size (5000mS say.) we then
 * set the pid to adjust its output between 0 and that window
 * size.  lastly, we add some logic that translates the PID
 * output into "Relay On Time" with the remainder of the
 * window being "Relay Off Time"
 ********************************************************/

#include <brettbeauregard_pid.h>

#define PIN_INPUT 0
#define RELAY_PIN 6

typedef double Real;

//Define Variables we'll be connecting to
Real setpoint;
constexpr uint32_t WINDOW_SIZE = 5000;
uint32_t windowStartTime;

Real calculateInput(uint32_t) {
  return analogRead(PIN_INPUT);
}

void processOutput(Real output) {

  /************************************************
   * turn the output pin on/off based on pid output
   ************************************************/
  while (millis() - windowStartTime > WINDOW_SIZE){
    //time to shift the Relay Window
    windowStartTime += WINDOW_SIZE;
  }
  // This assumes the relay is ON when signal is high.
  digitalWrite(RELAY_PIN, output > (millis() - windowStartTime));
}

//Specify the links and initial tuning parameters
Real kp=2, ki=5, kd=1;
brettbeauregard::PIDT<Real> myPID(calculateInput, processOutput, &setpoint, kp, ki, kd, brettbeauregard::DIRECT);

void setup(){
  windowStartTime = millis();

  // Set RELAY_PIN as output
  pinMode(RELAY_PIN,OUTPUT);
  
  //initialize the variables we're linked to
  setpoint = 100;

  //tell the PID to range between 0 and the full window size
  myPID.setOutputLimits(0, WINDOW_SIZE);

  // try to prevent integral windup
  myPID.setSampleTime(WINDOW_SIZE/4);

  //turn the PID on
  myPID.setMode(brettbeauregard::AUTOMATIC);
}

void loop() {
  myPID.compute();
}
