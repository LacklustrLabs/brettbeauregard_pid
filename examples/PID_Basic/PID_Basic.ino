/********************************************************
 * PID Basic Example using double instead of double
 * 
 * Reading analog input 0 to control analog PWM output 3
 ********************************************************/

#include <brettbeauregard_pid.h>

#define PIN_INPUT 0
#define PIN_OUTPUT 3

//Define Variables we'll be connecting to
double setpoint;

double calculateInput(unsigned long timeChange) {
  return analogRead(PIN_INPUT);
}

void processOutput(double output) {
  analogWrite(PIN_OUTPUT, output);
}

//Specify the links and initial tuning parameters
double kp=2, ki=5, kd=1;
brettbeauregard::PID myPID(calculateInput, processOutput, &setpoint, kp, ki, kd, brettbeauregard::DIRECT);

void setup(){
  setpoint = 100;

  //turn the PID on
  myPID.setMode(brettbeauregard::AUTOMATIC);
}

void loop(){
  myPID.compute();
}


